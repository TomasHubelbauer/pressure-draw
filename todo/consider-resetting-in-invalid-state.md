# Consider resettign in invalid state

When we pick up rendering from last cached image, it may be that we start with an action which relies on side effect of a previous action.
For example, `stroke-end` relies on `stroke-start` setting the scene by calling `beingPath` and moving to the correct coordinates.

We need to find a way to mark actions as resumeable from them. We should introduce a `isStandalone` boolean field on all actions.
When calling `cache`, do not cache for the last action, but for the last standalone action.
