# Improve transfer format

Do not use JSON (chatty), use a plan text format, like:

```csv
stroke-start,0,0,0.5
stroke-end,0,1
```

And ZIP it for transfer.
