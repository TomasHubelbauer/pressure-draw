import ActionBase from "./ActionBase";
import { State } from "..";

export default class StrokeStartAction extends ActionBase {
  public static readonly type = 'stroke-start';
  public readonly type = StrokeStartAction.type;
  public readonly isStandalone = true;
  public readonly x: number;
  public readonly y: number;
  constructor(x: number, y: number) {
    super();
    this.x = x;
    this.y = y;
  }
  public act(state: State, context: CanvasRenderingContext2D) {
    context.beginPath();
    context.moveTo(this.x, this.y);
  }
}
