import ActionType from "./ActionType";
import { State } from "..";

export default abstract class ActionBase {
  public abstract readonly type: ActionType;
  public abstract readonly isStandalone: boolean;
  public abstract act(state: State, context: CanvasRenderingContext2D): void;
}
