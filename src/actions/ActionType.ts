import SetThicknessAction from './SetThicknessAction';
import StrokeStartAction from './StrokeStartAction';
import StrokeEndAction from './StrokeEndAction';

type ActionType =
  | typeof SetThicknessAction.type
  | typeof StrokeStartAction.type
  | typeof StrokeEndAction.type
;

export default ActionType;
