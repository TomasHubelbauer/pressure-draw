import ActionBase from "./ActionBase";
import { State } from "..";

export default class SetThicknessAction extends ActionBase {
  public static readonly type = 'set-thickness';
  public readonly type = SetThicknessAction.type;
  public readonly isStandalone = true;
  public readonly thickness: number;
  constructor(thickness: number) {
    super();
    this.thickness = thickness;
  }
  public act(state: State, context: CanvasRenderingContext2D) {
    state.thickness = this.thickness;
  }
}
