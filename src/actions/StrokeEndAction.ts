import ActionBase from "./ActionBase";
import { State } from "..";

export default class StrokeEndAction extends ActionBase {
  public static readonly type = 'stroke-end';
  public readonly type = StrokeEndAction.type;
  public readonly isStandalone = false; // Depends on stroke-start
  public readonly x: number;
  public readonly y: number;
  public readonly pressure: number;
  constructor(x: number, y: number, pressure: number) {
    super();
    this.x = x;
    this.y = y;
    this.pressure = pressure;
  }
  public act(state: State, context: CanvasRenderingContext2D) {
    context.lineTo(this.x, this.y);
    context.closePath();
    context.lineCap = 'round';
    context.lineJoin = 'round';
    context.lineWidth = this.pressure * state.thickness;
    context.strokeStyle = 'black';
    context.stroke();
  }
}
