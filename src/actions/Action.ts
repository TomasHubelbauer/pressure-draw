import SetThicknessAction from "./SetThicknessAction";
import StrokeStartAction from "./StrokeStartAction";
import StrokeEndAction from "./StrokeEndAction";

type Action =
  | SetThicknessAction
  | StrokeStartAction
  | StrokeEndAction
;

export default Action;
