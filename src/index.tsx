/// <reference path='./index.d.ts'/>

import { h, mount, patch } from 'petit-dom';
import checkerboard from './img/checkerboard.png';
import Action from './actions/Action';
import SetThicknessAction from './actions/SetThicknessAction';
import StrokeStartAction from './actions/StrokeStartAction';
import StrokeEndAction from './actions/StrokeEndAction';

export type State = {
	thickness: number;
};

type Cache = {
	image: ImageData;
	state: State;
};

const actions: Action[] = [];
let actionIndex = -1;
let images: { [actionIndex: number]: Cache; } = {};

let state: State;

let app = <div />;
window.addEventListener('load', () => {
		act(new SetThicknessAction(10));
		document.body.appendChild(mount(app));
		render();
});

window.addEventListener('resize', () => {
	// Drop all caches as the canvas size has changed!
	images = {};
	render();
});

function render() {
	const temp = app;
	app =
			<div style="display: flex; flex: 1; overflow: hidden;">
				<div style="display: flex; flex-direction: column; width: 300px;">
					<div>
						<button onclick={onSavePngButtonClick}>Save PNG</button>
						<button onclick={onSaveJsonButtonClick}>Save JSON</button>
						<button onclick={onUndoButtonClick}>Undo</button>
						<button onclick={onRedoButtonClick}>Redo</button>
					</div>
					<div>
						<div>Last render: {new Date().toLocaleString()}</div>
						<div>Action index: {actionIndex}</div>
					</div>
					<div style="flex: 1; overflow: auto;">

					</div>
				</div>
				<div id="sizeDiv" style="flex: 1; overflow: hidden;">
					<canvas
						id="paintCanvas"
						onpointerdown={onCanvasPointerDown}
						onpointermove={onCanvasPointerMove}
						onpointerup={onCanvasPointerUp}
						style={`background: url(${checkerboard});`}/>
				</div>
			</div>;
	patch(app, temp);

	const paintCanvas = document.querySelector<HTMLCanvasElement>('#paintCanvas');
	const sizeDiv = document.querySelector<HTMLDivElement>('#sizeDiv');
	const { height, width } = sizeDiv.getBoundingClientRect();
	paintCanvas.height = height;
	paintCanvas.width = width;

	if (actions.length === 0) {
		return;
	}

	const context = paintCanvas.getContext('2d');
	state = {
		thickness: 0,
	};

	for (let index = actionIndex; index > 0; index--) {
		if (images[index] !== undefined) {
			const { image, state: cachedState } = images[index];
			if (image.height === height && image.width === width) {
				console.log(`Picking up rendering from cached action at index ${index} and rendering ${actions.length - index} remaining actions from scratch!`);
				context.putImageData(image, 0, 0);
				state = cachedState;
				for (index; index < actionIndex; index++) {
					actions[index].act(state, context);
				}
				return;
			} else {
				images[index] = undefined;
			}
		}
	}

	console.log(`Rendering ${actions.length} actions from scratch!`);
	for (let index = 0; index < actionIndex; index++) {
		actions[index].act(state, context);
	}
}

function act(action: Action) {
	// TODO: Ditch the rest of the array from actionPoint onwards after undo has been done and now that a new action occurs (keep in the meantime for redo)
	actionIndex++;
	actions[actionIndex] = action;	
}

function cache() {
	const paintCanvas = document.querySelector<HTMLCanvasElement>('#paintCanvas');
	const sizeDiv = document.querySelector<HTMLDivElement>('#sizeDiv');
	const { height, width } = sizeDiv.getBoundingClientRect();
	// TODO: Cache the last isStandalone action, not the last action always!
	images[actionIndex] = { image: paintCanvas.getContext('2d').getImageData(0, 0, width, height), state };
}

function onSavePngButtonClick(event: MouseEvent) {
	const timestamp = (new Date()).toISOString().replace('T', '-').replace(/:/g, '-');
	const downloadA = document.createElement('a');
	downloadA.download = `${timestamp}.png`;
	const paintCanvas = document.querySelector<HTMLCanvasElement>('#paintCanvas');
	downloadA.href = paintCanvas.toDataURL();
	document.body.appendChild(downloadA);
	downloadA.click();
	document.body.removeChild(downloadA);
}

function onSaveJsonButtonClick(event: MouseEvent) {
	const timestamp = (new Date()).toISOString().replace('T', '-').replace(/:/g, '-');
	const downloadA = document.createElement('a');
	downloadA.download = `${timestamp}.json`;
	downloadA.href = 'data:application/json;charset=utf8,' + encodeURIComponent(JSON.stringify({ actions, actionIndex, timestamp: new Date() }, null, 2));
	document.body.appendChild(downloadA);
	downloadA.click();
	document.body.removeChild(downloadA);
}

function onUndoButtonClick(event: MouseEvent) {
	// TODO: Check bounds
	actionIndex--;
	render();
}

function onRedoButtonClick(event: MouseEvent) {
	// TODO: Check bounds
	actionIndex++;
	render();
}

function onCanvasPointerDown(event: PointerEvent) {
	act(new StrokeStartAction(event.offsetX, event.offsetY));
	render();
}

function onCanvasPointerMove(event: PointerEvent) {
	if (event.buttons === 1) {
		act(new StrokeEndAction(event.offsetX, event.offsetY, event.pressure));
		act(new StrokeStartAction(event.offsetX, event.offsetY));
		render();
	}
}

function onCanvasPointerUp(event: PointerEvent) {
	act(new StrokeEndAction(event.offsetX, event.offsetY, event.pressure));
	cache();
	render();
}

function onViewCacheImageButtonClick(event: MouseEvent) {
	const index = Number((event.currentTarget as HTMLButtonElement).dataset['index']);
	if (images[index] === undefined) {
		alert('No cached image for this index');
		return;
	}

	// TODO: Use OffscreenCanvas here
	const { image } = images[index];
	const canvas = document.createElement('canvas');
	canvas.width = image.width;
	canvas.height = image.height;
	const context = canvas.getContext('2d');
	context.putImageData(image, 0, 0);
	window.open(canvas.toDataURL());
}

function onDropCacheImageButtonClick(event: MouseEvent) {
	const index = Number((event.currentTarget as HTMLButtonElement).dataset['index']);
	if (images[index] === undefined) {
		alert('No cached image for this index');
		return;
	}

	images[index] = undefined;
	render();
}
