# FireFox Pressure

https://github.com/thenickdude/chickenpaint/blob/master/help/Firefox%20pressure%20support.md

- `about:config`
- `dom.w3c_pointer_events.enabled` = `true`
- `dom.w3c_pointer_events_dispatch_by_pointer_messages` = `true`

This seems to be enabled for me?!
